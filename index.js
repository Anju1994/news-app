
let mobilemenu=document.querySelector(".mobile")
let menubtn=document.querySelector(".menubtn")
let menubtnDisplay=true;
menubtn.addEventListener("click",()=>{
    mobilemenu.classList.toggle("hidden")
})

const API_KEY="cdad33b81de34c5c8b3db5474deb4db1";
const url="https://newsapi.org/v2/everything?q=";
window.addEventListener("load", () => fetchNews("Technology"));
async function fetchNews(query)
{
 const res= await fetch(`${url}${query}&apiKey=${API_KEY}`);
 const data = await res.json();
 bindData(data.articles);
}
function bindData(articles)
{
    const cardsContainer = document.getElementById('cardscontainer');
    const newsCardTemplate = document.getElementById('template-news-card');
   
    cardsContainer.innerHTML="";
    articles.forEach((article) =>{
        if (!article.urlToImage) return;
       const cardClone = newsCardTemplate.content.cloneNode(true);
       fillDataInCard(cardClone,article);
       cardsContainer.appendChild(cardClone);
    })
}

function fillDataInCard(cardClone, article){
    const newsImg=cardClone.querySelector('#news-img');
    
    const newsTitle=cardClone.querySelector('#news-title');

    const newsSource=cardClone.querySelector('#news-source');
    const newsDesc=cardClone.querySelector('#newsdesc');
    
    newsImg.src = article.urlToImage;
    //console.log(newsImg);
    newsTitle.innerHTML = `${article.title.slice(0,60)}...`;
    //console.log(newsTitle);
    newsDesc.innerHTML = `${article.description.slice(0,250)}...`;
    //console.log(newsDesc);
    const date = new Date(article.publishedAt).toLocaleString("en-US",{timeZone:"Asia/Jakarta"})
    newsSource.innerHTML = `${article.source.name}.${date}`;

}
/*const searchBtn = document.getElementById("searchForm")
const searchBtnMobile = document.getElementById("searchFormMobile")
//const searchInputMobile = document.getElementById("searchInputMobile")
const searchInput = document.getElementById("searchInput")
 searchBtn.addEventListener("submit",async(e)=>{
    e.preventDefault()
    console.log(searchInput,value)
     const data = await fetchNews(searchInput.value)
     bindData(data.articles)
 })
searchBtnMobile.addEventListener("submit", async(e)=>{
    e.preventDefault()
    const data = await fetchNews(searchFormMobile.value)
    bindData(data.articles)
})
async function Search(query){
    console.log("script loaded")
    const data = await fetchNews(query)
    bindData(data.articles)
}*/